import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApiService, PostService, UserService, NotificationsService } from './services';

@NgModule({
  imports: [
    CommonModule, RouterModule, BrowserAnimationsModule
  ],
  exports: [CommonModule, RouterModule, BrowserAnimationsModule],
  declarations: [],
  providers: [ApiService, PostService, UserService, NotificationsService]
})
export class CoreModule { }
