import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';
import { Observable } from '../../../../node_modules/rxjs/Observable';
import { NotificationsService } from './notification.service';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-type': 'application/json; charset=UTF-8'
    })
};

const baseUrl = 'https://jsonplaceholder.typicode.com';

@Injectable()
export class ApiService {
    constructor(private http: HttpClient, private notification: NotificationsService) { }

    public get<T>(url: string): Observable<T> {
        return this.http.get<T>(`${baseUrl}${url}`, httpOptions).pipe(
            catchError(error => this.handleError(error))
        );
    }

    public post<T>(url, data: T): Observable<T> {
        return this.http.post<T>(`${baseUrl}${url}`, data, httpOptions)
            .pipe(
                catchError(error => this.handleError(error))
            );
    }

    public delete(url: string): Observable<{}> {
        return this.http.delete(`${baseUrl}${url}`, httpOptions)
            .pipe(
                catchError(error => this.handleError(error))
            );
    }

    public put<T>(url, data: T): Observable<T> {
        return this.http.put<T>(`${baseUrl}${url}`, data, httpOptions)
            .pipe(
                catchError(error => this.handleError(error))
            );
    }

    private handleError(error: HttpErrorResponse) {
        this.notification.error('Some error was happened');
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        return new ErrorObservable(
            'Something bad happened; please try again later.');
    }
}
