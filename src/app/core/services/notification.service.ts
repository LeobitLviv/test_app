import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

export enum NotificationStatus {
    success = 'notification-success',
    info = 'notification-info',
    warning = 'notification-warning',
    error = 'notification-error'
}

@Injectable()
export class NotificationsService {
    constructor(private snackbar: MatSnackBar) { }

    open(message: string, duration = 2000, status: NotificationStatus, action = ''): void {
        this.snackbar.open(message, action, {
            duration: duration,
            extraClasses: [status]
        });
    }

    success(message, duration = 2000, action = ''): void {
        this.open(message, duration, NotificationStatus.success, action);
    }

    info(message, duration = 2000, action = ''): void {
        this.open(message, duration, NotificationStatus.info, action);
    }

    warning(message, duration = 2000, action = ''): void {
        this.open(message, duration, NotificationStatus.warning, action);
    }

    error(message, duration = 2000, action = ''): void {
        this.open(message, duration, NotificationStatus.error, action);
    }
}
