import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Post } from '../../shared/models';

@Injectable()
export class PostService {

    constructor(private api: ApiService) { }

    public getPosts() {
        return this.api.get<Post[]>('/posts');
    }

    public getPost(id: number) {
        return this.api.get<Post>(`/posts/${id}`);
    }

    public createPost(post: Post) {
        return this.api.post<Post>('/posts', post);
    }

    public updatePost(post: Post) {
        return this.api.put<Post>(`/posts/${post.id}`, post);
    }

    public deletePost(id: number) {
        return this.api.delete(`/posts/${id}`);
    }
}
