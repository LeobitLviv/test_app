import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { User } from '../../shared/models';

@Injectable()
export class UserService {

    constructor(private api: ApiService) { }

    getUsers() {
        return this.api.get<User[]>('/users');
    }

    getUser(id: number) {
        return this.api.get<User>(`/users/${id}`);
    }
}
