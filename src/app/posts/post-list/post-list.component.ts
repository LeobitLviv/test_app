import { Component, OnInit } from '@angular/core';
import { PostService, UserService, NotificationsService } from '../../core/services';
import { Post, User } from '../../shared/models';
import { zip } from '../../../../node_modules/rxjs/operators';
import { MatDialog } from '@angular/material';
import { DeleteConfirmModal } from '../../shared/modals/delete-confirm/delete-confirm.modal';
import { PostEditModal } from '../../shared/modals/post-edit/post-edit.modal';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

  constructor(private postService: PostService, private userService: UserService,
    private dialog: MatDialog, private notification: NotificationsService) { }

  public posts: Post[];
  public users: User[];
  public spinner = false;

  ngOnInit() {
    this.showSpinner();
    this.postService.getPosts().pipe(zip(this.userService.getUsers())).subscribe(([posts, users]) => {
      posts.forEach(post => {
        const user = users.find(u => u.id === post.userId);
        if (user) {
          post.user = user;
        }
      });
      this.posts = posts;
      this.users = users;
      this.hideSpinner();
    });
  }

  openDialog(post: Post): void {
    const dialogRef = this.dialog.open(DeleteConfirmModal, {
      width: '350px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.showSpinner();
        this.postService.deletePost(post.id).subscribe(data => {
          this.posts = this.posts.filter(p => p.id !== post.id);
          this.notification.success('Post is deleted successfully');
          this.hideSpinner();
        }, () => this.hideSpinner());
      }
    });
  }

  openEdit(post: Post): void {
    const dialogRef = this.dialog.open(PostEditModal, {
      width: '500px',
      data: { post: post ? post : {}, users: this.users }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.showSpinner();
        if (post) {
          this.postService.updatePost(result).subscribe(data => {
            const postToUpdate = this.posts.find(p => p.id === post.id);
            Object.assign(postToUpdate, data);
            postToUpdate.user = this.users.find(user => postToUpdate.userId === user.id);
            this.notification.success('Post is updated successfully');
            this.hideSpinner();
          }, () => this.hideSpinner());
        } else {
          this.postService.createPost(result).subscribe(data => {
            data.user = this.users.find(user => data.userId === user.id);
            this.posts.unshift(data);
            this.notification.success('Post is created successfully');
            this.hideSpinner();
          }, () => this.hideSpinner());
        }
      }
    });
  }

  private hideSpinner() {
    this.spinner = false;
  }

  private showSpinner() {
    this.spinner = true;
  }

}
