import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '../../../node_modules/@angular/forms';
import { FlexLayoutModule } from '../../../node_modules/@angular/flex-layout';

import { PostsRoutingModule } from './posts-routing.module';
import { PostListComponent } from './post-list/post-list.component';
import { SharedModule } from '../shared/shared.module';
import { DeleteConfirmModal } from '../shared/modals/delete-confirm/delete-confirm.modal';
import { PostEditModal } from '../shared/modals/post-edit/post-edit.modal';

@NgModule({
  imports: [
    CommonModule,
    PostsRoutingModule,
    SharedModule,
    FlexLayoutModule,
    ReactiveFormsModule
  ],
  declarations: [PostListComponent, DeleteConfirmModal, PostEditModal],
  entryComponents: [DeleteConfirmModal, PostEditModal]
})
export class PostsModule { }
