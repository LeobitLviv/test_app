import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    templateUrl: 'delete-confirm.modal.html',
})
export class DeleteConfirmModal {

    constructor(
        public dialogRef: MatDialogRef<DeleteConfirmModal>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    onNoClick(): void {
        this.dialogRef.close(false);
    }

}
