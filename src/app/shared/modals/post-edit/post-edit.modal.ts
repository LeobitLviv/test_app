import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Post, User } from '../../models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    templateUrl: 'post-edit.modal.html',
    styleUrls: ['post-edit.modal.scss']
})
export class PostEditModal {

    public postForm: FormGroup

    constructor(
        public dialogRef: MatDialogRef<PostEditModal>,
        @Inject(MAT_DIALOG_DATA) public data: {post: Post, users: User[]}, private fb: FormBuilder) {
            this.createForm();
        }

    onNoClick(): void {
        this.dialogRef.close(false);
    }

    submit(model: FormGroup): void {
        this.dialogRef.close(model.value);
    }

    createForm() {
        this.postForm = this.fb.group({
            id: [this.data.post.id],
            title: [this.data.post.title, Validators.required],
            body: [this.data.post.body, Validators.required],
            userId: [this.data.post.userId, Validators.required],
        });

    }
}
